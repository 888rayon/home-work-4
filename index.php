<?php

declare(strict_types=1);

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('log_errors', 'On');
ini_set('error_log', 'php_errors.log');

require_once __DIR__ . '/vendor/autoload.php';

use aquiver\exception\BaseException;
use aquiver\task_37\Date;
use aquiver\task_36\User;
use aquiver\task_41\ShutDown;

// Task 36
echo '<h2>Task 36</h2>';
$user = new User('Taras', 'Potochilov', 'Sergeevich');
echo '<p>' . $user . '</p>';

// Task 37
echo '<h2>Task 37</h2>';
$date = new Date('2020', '02', '04');
// create new properties for class Date
$date->propertiesOne = 'undefined value';
$date->propertiesTwo = 33;

// show week day by date
$date->weekDay = 'day';
echo '<p>' . $date->weekDay . '</p>';

// reserved methods only featureMethod and featureMethodTwo
echo '<p>' . $date->featureMethod() . '</p>';
echo '<p>' . $date->featureMethodTwo() . '</p>';

// Task 38
try {
    $config = "test.php";
    if ( ! file_exists($config)) {
        throw new BaseException("My custom exception", 666);
    }
} catch (BaseException  $e) {
    echo $e->getMessage();
}

// Task 41
require 'error.php';
$shutdown = new ShutDown();
